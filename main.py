#!/usr/bin/env python
# -*- encoding: utf-8 -*-
from __future__ import unicode_literals
import xml.etree.ElementTree as ET
import dataset
import os


# Source file for MythTV config
conf_file = '/etc/mythtv/config.xml'
# Target folder in which to link the files
target_folder = '/media/varasto/Videot/MythTV'

# Parse MythTV config
tree = ET.parse(conf_file)
root = tree.getroot()
# Find DefaultBackend and set variables
for child in root.iter('DefaultBackend'):
    db_host = child.find('DBHostName').text
    db_user = child.find('DBUserName').text
    db_password = child.find('DBPassword').text
    db_name = child.find('DBName').text
    db_port = int(child.find('DBPort').text)

# Connect to database
db = dataset.connect('mysql://%s:%s@%s:%s/%s' % (db_user, db_password, db_host, db_port, db_name))
# Find list of storage directories ignoring LiveTV
storage_dirs = [r['dirname'] for r in db['storagegroup'].all() if r['dirname'] != 'LiveTV']
# Recordings table
record_table = db['recorded']

# If the target folder doesn't exists, create it (recursively)
if not os.path.exists(target_folder):
    os.makedirs(target_folder)

# Create list of files found for later use
files_found = []
# Loop through storage directories
for d in storage_dirs:
    # And all their files
    for f in os.listdir(d):
        # Don't link anything but .mpg-files
        if not f.endswith('.mpg'):
            continue
        # Find info for the file
        info = record_table.find_one(basename=f)
        # If not found, don't add it
        if not info:
            continue
        # Create filename
        f_name = '%s - %s.mpg' % (info['starttime'], info['title'].decode('iso-8859-15'))
        # Add to found files
        files_found.append(f_name)
        # Create link
        link_source = os.path.join(d, f)
        link_target = os.path.join(target_folder, f_name)
        try:
            os.symlink(link_source, link_target)
        except OSError:
            pass

# Find all files in target_dir
for f in os.listdir(target_folder):
    path = os.path.join(target_folder, f)
    # if they're not in database (and are symlinks) -> remove
    if f not in files_found and os.path.islink(path):
        os.unlink(path)
